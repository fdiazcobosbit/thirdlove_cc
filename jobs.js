const config = require('./config/config.js');
const db = require('./db')

// Jobs that deletes carts without an order ID
const remove_old_carts = function () {
    return setInterval(async function () {
        let carts = await db.listDocuments('carts');
        carts.forEach(cart => {
            if (cart.order_id == null) {
                db.removeDocument('carts', cart.id)
            }
        });
    }, config.DELETE_OLD_CARTS_INTERVAL);
}

module.exports = {remove_old_carts}
