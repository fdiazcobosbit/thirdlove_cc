FROM node
COPY . /opt/thirdlove
WORKDIR /opt/thirdlove
EXPOSE 80
RUN npm install
ENTRYPOINT node ./bin/www