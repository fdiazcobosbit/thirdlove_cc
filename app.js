const express = require('express');
const events = require('events');
const emitter = new events.EventEmitter();
const initDb = require('./db').initDb;
const jobs = require('./jobs');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const path = require('path');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const cartRouter = require('./routes/cart');
const catalogRouter = require('./routes/catalog');
const usersRouter = require('./routes/user');
const checkoutRouter = require('./routes/checkout');
let cartpurge = null;

const app = express();
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Initialize connection once
emitter.on('initalizedb', function () {
    initDb(emitter);
})

emitter.on('initalizeRoutes', function () {
    app.use('/', indexRouter);
    app.use('/cart', cartRouter);
    app.use('/catalog', catalogRouter);
    app.use('/user', usersRouter);
    app.use('/checkout', checkoutRouter);

    emitter.emit('initalizeApp')
})

emitter.on('initalizeApp', function () {
    cartpurge = jobs.remove_old_carts();
    app.use(express.static(path.join(__dirname, 'public')));
})

emitter.emit('initalizedb')

module.exports = app