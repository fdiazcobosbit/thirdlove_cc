# thirdlove_CC

## How to install it

The app is hosted in AWS EC2 Compute instance with IP: 34.227.11.105

But if you want to install it by yourself, you just need to have Docker :)
You just need to run
`docker-compose up -d`

And a mongoDB and the nodejs app will be deployed, and it will be exposed in port 80

## How to see the docuemntation of the API

You have two ways using swagger at:

http://34.227.11.105/api-docs/

or import by yourself the Postman collection postman-collection.json

## Test

The app includes simple HTTP response codes tests, made it with newman (the Postman lib)

to run it, you just need to run:


`newman run postman-collection.json` or `npm tests`

![tests.png](./public/images/tests.png)

## Simple architecture:

![arch.png](/public/images/arch.png)


## Brief report and code guidelines i followed.

I allways try to follow the 12 Factors Apps for cloud applications, of course I could not followed all in this application due to size/time limitations.

https://12factor.net/

And use the guideline of https://github.com/airbnb/javascript 

Also I like Docker and i think for micro services is a great tool, so the app can run in a container, or docker-compose or docker-stack or Kubernetes

I did not use uncommon libraries, I tried to use the most common ones, because is not a silver bullet, but allways a common library has more support from the community and that means more manteinability.


So I used:
 *   "axios": for HTTP Calls
 *   "express": as the framework
 *   "mongodb": for the mongo driver
 *   "newman": the postman library to run simple tests
 *   "swagger-ui-express": to document the API


I tried to use NodeJs Events and asyc and awaits statements over thens or callbacks.

I did not use mongoose, i think that the mongodb driver for node is very good and faster than moongoose.



## Requirements
	• Create a (set of) microservices that provide:
		• Shopping Cart functionality (list, creation, modification, deletion).
		• Checkout functionality (submission).
		• A job that deletes from the database the carts that after a certain time -configurable-  don't have a linked order
	• Add authentication to the checkout endpoint only.
	• The product catalog should show only products for which there is at least one variant with stock in the Shopify store.
	• Creation of a cart should fail if the product doesn't exist in Shopify.
	• When a checkout is done create that order in Shopify (https://help.shopify.com/en/api/reference/orders/order#create)
	• The checkout should fail if the product is out of stock.
	• It's not required to have a payment when creating the Order, but you are free to add it if you want.
	• Expose a static site with the documentation of the API

Presentation
	• The exercise is estimated to be completed in 9 days so the day after you should be sending us your work to cordoba-candidates@thirdlove.com. The deadline can be extended if you'd like to do additional things from those we ask you to do.
	• If you have any questions or if you feel like you need some clarification, don’t hesitate to reach us.
	• Create a repository in Bitbucket to version the code.
	• Create a running environment (AWS/Heroku/Google Cloud/DigitalOcean)
	• The implementation needs to be production ready.
	• Write a brief report explaining which technologies/libraries/patterns you used and why you chose them. If needed, include comments to the code to help us understand how it was implemented.

What we expect to see:
	• A production-ready code, which has all the elements that you would include in a piece of code you'd deliver at your current job.
	• A node (javascript) code base.
What we will love to see in your work
	• Thoughtful and properly communicated criteria for design decisions.
	• Good balance between "not assuming things" and "common sense"