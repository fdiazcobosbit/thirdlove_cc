const config = require('./config/config.js');
const MongoClient = require('mongodb').MongoClient;
const mongoUrl = 'mongodb://' + config.MONGO_USERNAME + ':' + config.MONGO_PASSWORD + '@' + config.MONGO_HOST + ':' + config.MONGO_PORT + '/admin';
let db;

function initDb(emitter) {
    if (db) {
        console.warn('Trying to init DB again!');
        return db;
    }
    MongoClient.connect(mongoUrl, { useNewUrlParser: true, poolSize: 10 }, function (err, database) {
        if (err) throw err;
        db = database.db(config.MONGO_DATABASE);;
        console.log('Mongo is up and running');
        emitter.emit('initalizeRoutes')
    })
}

function getDb() {
    return db;
}

const findDocumentsById = async function (documents, id) {
    // Get a document from the collection
    db = getDb();
    const collection = db.collection(documents);
    // Find some documents
    const data = await collection.find({ id }).toArray();

    return data[0];
}

const listDocuments = async function (documents) {
    // Get  all the documents from a collection
    db = getDb();
    const collection = db.collection(documents);
    // Find some documents
    const data = await collection.find();

    return data.toArray();
}

// Upsert Create or update
const upsertDocument = async function (documents, document) {
    db = getDb()
    // Get the documents collection
    const collection = db.collection(documents);
    // Insert some documents
    var newobj = { $set: document }
    const data =  await collection.updateOne(
        { id: document.id },
        newobj, { upsert: true });
    return data.result;
}

const removeDocument = async function (documents, id) {
    db = getDb()
    // Get the documents collection
    const collection = db.collection(documents);

    const ret = await collection.deleteOne({ id });
    return ret.result
}

module.exports = {
    getDb,
    initDb,
    removeDocument,
    upsertDocument,
    findDocumentsById,
    listDocuments
};