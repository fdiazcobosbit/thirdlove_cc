// According with 12 factors app for web saas apps https://12factor.net/es/config all configuration shuld be in the environment.

// APP
const PORT = process.env.PORT || '80';

// JOBS
const DELETE_OLD_CARTS_INTERVAL = process.env.DELETE_OLD_CARTS_INTERVAL = 60 * 60 * 1000;

// DB
const MONGO_USERNAME = process.env.MONGO_USERNAME || 'thirdlove';
const MONGO_PASSWORD = process.env.MONGO_PASSWORD || 'thirdlove';
const MONGO_HOST = process.env.MONGO_HOST || '127.0.0.1'
const MONGO_PORT = process.env.MONGO_PORT || '27017';
const MONGO_DATABASE = process.env.MONGO_DATABASE || 'admin'

// SHOPIFY
const SHOPIFYHOST = 'thirdlove-uat2.myshopify.com'
// In production these keys should be passes via env vars
const SHOPIFY_API_KEY = process.env.SHOPIFY_API_KEY || '7d2c8bd2c922f1be3bc7806e3f89a68a';
const SHOPIFY_PASSWORD = process.env.SHOPIFY_PASSWORD || 'af3acc3b4f168d2740bd7c1dc3ea1b70';
const SHOPIFY_BASE_ADDRESS = `https://${SHOPIFY_API_KEY}:${SHOPIFY_PASSWORD}@${SHOPIFYHOST}/`;

module.exports = {
    PORT,
    DELETE_OLD_CARTS_INTERVAL,
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOST,
    MONGO_PORT,
    MONGO_DATABASE,
    SHOPIFYHOST,
    SHOPIFY_API_KEY,
    SHOPIFY_BASE_ADDRESS,
    SHOPIFY_PASSWORD
}
