const express = require('express');
const router = express.Router();
const db = require('../db')

router.get('/', async function (req, res) {
    var user_id = req.param('id');
    try {
        if (user_id != null) {
            const user = await db.findDocumentsById('users', user_id);
            res.send(200, user);
        }
        else {
            const users = await db.listDocuments('users');
            res.send(200, users)
        }
    }
    catch (err) {
        res.send(400, err)
    }
});

router.delete('/', async function (req, res) {
    var user_id = req.param('id');
    try {
        if (user_id != null) {
            const ret = await db.removeDocument('users', user_id);
            res.send(200, ret)
        }
        else {
            res.send(400, 'id not present in query param')
        }
    }
    catch (err) {
        res.send(400, err)
    }
});

router.put('/', async function (req, res) {
    const { id: user_id, username, password, email } = req.body;
    try {
        if (!(user_id || username || password || email )) {
            res.send(400, 'Invalid user')
        }
        else {
            const ret = await db.upsertDocument('users', req.body);
            res.send(200, ret)
        }
    }
    catch (err) {
        res.send(400, err)
    }
});

module.exports = router;