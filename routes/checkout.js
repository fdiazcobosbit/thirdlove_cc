const express = require('express');
const router = express.Router();
const db = require('../db')
const axios = require('axios');
const config = require('../config/config.js');

router.post('/', async function (req, res) {
    const user_id = req.body.user_id

    if (! (req.body.password || req.body.cart_id || user_id)) {
        sendErrorPage(res, 'Invalid checkout values')
        return;
    }
    const user = await db.findDocumentsById('users', user_id);
    const cart = await db.findDocumentsById('carts', req.body.cart_id);
    // authenticate user
    if (!user || user.password != req.body.password) {
        sendErrorPage(res, 'Invalid user/password')
        return;
    }
    // Check that the cart_id is from the user
    if (!cart || cart.user_id != user_id) {
        sendErrorPage(res, 'invalid cart ID')
        return;
    }
    // validate stock
    if (!checkStock(cart)) {
        sendErrorPage(res, 'They are not stock for the selected items')
        return;
    }

    let order = {};
    order.order = {};
    let line_items = cart.products;
    order.order.line_items = line_items;
    try {
        response = await axios.post(config.SHOPIFY_BASE_ADDRESS + '/admin/orders.json', order)
        if (response.data) {
            cart.order_id = response.data.order.id
            db.upsertDocument('carts', cart);
            res.send(201)
        }
    }
    catch(err) {
        console.log(err);
        sendErrorPage(res, 'Problem creating order: ' + err)
    };
});

const checkStock = async function (cart) {
    for (var product of cart.products) {
        try {
            resp = await axios.get(config.SHOPIFY_BASE_ADDRESS + '/admin/variants/' + product.variant_id + '.json')
            if (resp.inventory_quantity < product.quantity) {
                return false;
            }
        }
        catch (err) {
            return false;
        }
    }
    return true;
}

const sendErrorPage = function (res, msg) {
    res.send(400, msg);
}

module.exports = router;
