const express = require('express');
const router = express.Router();
const axios = require('axios');
const db = require('../db')
const config = require('../config/config.js');

router.get('/', async function (req, res) {
    try {
        var cart_id = req.param('id');
        if (cart_id != null) {
            const cart = await db.findDocumentsById('carts', cart_id);
            res.send(200, cart);
        }
        else {
            const carts = await db.listDocuments('carts');
            res.send(200, carts)
        }
    }
    catch (err) {
        res.send(400, err)
    }
});

router.delete('/:id', async function (req, res) {
    try {
        var cart_id = req.param('id');
        if (cart_id != null) {
            const ret = await db.removeDocument('carts', cart_id);
            res.send(200, ret)
        }
        else {
            sendErrorPage(res, 'id not present in query param')
        }
    }
    catch (err) {
        res.send(400, err)
    }
});

router.put('/', async function (req, res) {
    try {
        var products_variants = req.body.products;
        are_valid_products = await validProducts(products_variants);

        if (!are_valid_products) {
            sendErrorPage(res, 'Invalid producs variants')
            return;
        }

        var cart_id = req.body.id;
        var user_id = req.body.user_id;
        var date = Date.now()

        cart = req.body
        cart['date'] = date
        if (!cart_id || !user_id || !products_variants) {
            sendErrorPage(res, 'Invalid cart parameters')
        }
        else {
            const ret = await db.upsertDocument('carts', cart);
            res.send(200, ret)
        }

    }
    catch (err) {
        res.send(400, err)
    }
});

const validProducts = async function (products) {
    for (var product of products) {
        try {
            product = await axios.get(`${config.SHOPIFY_BASE_ADDRESS}/admin/variants/${product.variant_id}.json`)
        }
        catch (err) {
            if (err.response && err.response.status === 404) {
                return false;
            }
            logger.err(err)
        }
    }
    return true;
}

const sendErrorPage = function (res, msg) {
    res.send(400, msg);
}

module.exports = router;
