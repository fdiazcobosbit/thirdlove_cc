const express = require('express');
const router = express.Router();
const axios = require('axios');
const config = require('../config/config.js');
const PRODUCTS_PATH = 'admin/products.json'

router.get('/', async function (req, res, next) {
  let resp = ''
  let products_with_stock =  []
  try {
    const response = await axios.get(config.SHOPIFY_BASE_ADDRESS + PRODUCTS_PATH)
    let resp = response.data.products
  
    // need to show only products with at least one variant with stock
    for (let i = 0, len = resp.length; i < len; i++) {
      for (let j = 0, len = resp[i].variants.length; j < len; j++) {
        if (resp[i].variants[j].inventory_quantity >= 0) {
          products_with_stock.push(resp[i])
          break;
        }
      }
    }
  } catch (err) {
    logger.err(err)
    resp = err;
  }
  res.send(products_with_stock);
});

module.exports = router;